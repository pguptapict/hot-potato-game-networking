/*
* Project 3: Hot Potato
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <errno.h>
#include <time.h>
#define LEN	64
#define INVALID_SOCKET -1

//Function Declaration
size_t sendSize(char *str,int s);
size_t receiveSize(int s);
ssize_t readn(int fd, char **vptr);
ssize_t writen(int fd, char *vptr, size_t n);
int createSocket();
int startListening(int s);
int connectTo(char *host,int masterPort);
int playerNo,leftListenPort,rightListenPort,leftPlayerNo,rightPlayerNo;
int leftConnectSocket,rightConnectSocket;
int leftReadFd,rightReadFd;
char *leftHostName, *rightHostName;
void parseString(char *buf);
void connectToNeighbours();
void startGame(int connectSocket,int masterPort,char *argv);
void sendPotatoToPlayer(int side,char *potatoBuf);
void receiveRequestFromNeighbours(int listenSocket);


//Main method
int main (int argc, char *argv[]){
	char host[64];
	char *buf;
	
	int p, fp,len, masterPort, leftPort, rightPort;
  	int connectSocket,listenSocket,listenPort;
  	struct sockaddr_in sin,incoming;
  	int fds[2];
	char str[LEN];
	char sstr[LEN];
	int size,z;

	/* read master port number from command line */
  	if ( argc < 3 ) {
   		fprintf(stderr, "Usage: %s <master-machine-name> <port-number> \n", argv[0]);
    		exit(1);
  	}
	
	masterPort = atoi(argv[2]);
	listenSocket = createSocket();			//Create the listening socket.
	listenPort = startListening(listenSocket);	//Begin listening.
	fds[0] = listenSocket;

	connectSocket = connectTo(argv[1],masterPort);  //Connect to master. argv[1] is the hostname. 
	sprintf(sstr,"%d",listenPort);
	len = writen(connectSocket,sstr,strlen(sstr));  //Send the listening port to the master.
	
	len = readn(connectSocket,&buf);   //Read the connection info send by the master
	buf[len] = '\0';
	//printf("Buf is :: %s",buf);
	
	//Parse the connection Info
	parseString(buf);  
	free(buf);

	//Establish connection with neighbours.	
	connectToNeighbours();
	receiveRequestFromNeighbours(listenSocket);

	//Start the Game
	startGame(connectSocket,masterPort,argv[1]);
	printf("Game Finished");
	free(leftHostName);
	free(rightHostName);
	close(connectSocket);
  	exit(0);
}


/*
* Function to begin listening for potato from master/player. 
*
* Input  : s(Socket descriptor of the player), masterPort , argv
*
* Output : Return on completion of the Game.
*/
void startGame(int connectSocket,int masterPort,char *argv){
	int fds[2],random_number,fd,isClose=0,hopsCount,len,choice=9999,p,len1;
	struct sockaddr_in sin,incoming;
	char *pch;
	char *buf,*temp;
	char *potatoBuf;
	int lastSocket;
	fds[0] = connectSocket;	
	fds[1] = leftReadFd;
	fds[2] = rightReadFd;
	while(1){
		fd = network_accept_any(fds,3,(struct sockaddr *)&incoming, &len);
		if(fd == connectSocket)
			choice = 0;
		else if(fd == leftReadFd || fd == rightReadFd)
			choice = 1;
		switch(choice){
			case 0:
				len = readn(connectSocket,&buf);
				buf[len] = '\0';
     				if ( !strcmp("close", buf)){
					//close(connectSocket);
					//close(lastSocket);					
					exit(1);
			  	}else{
					temp = buf;
					pch = strtok (temp,":");
 					hopsCount = atoi(pch);
					pch = strtok(NULL,":");
					hopsCount--;
					if(hopsCount <= 0){
						isClose = 1;
						break;
					}
					
					if(pch == NULL)
						pch = "";
					potatoBuf = (char *)malloc( sizeof(char) * (10) );
					sprintf(potatoBuf,"%d:%s%d:",hopsCount,pch,playerNo);
					random_number = rand() % 2;
					if(random_number == 0)	
						printf("Sending potato to %d\n",leftPlayerNo);
					else
						printf("Sending potato to %d\n",rightPlayerNo);	
					
					sendPotatoToPlayer(random_number,potatoBuf);
					free(buf);
					free(potatoBuf);
					fflush(stdout);
				}
				break;
				
			case 1:		len1 = sizeof( (struct sockaddr *) &incoming);
					if(fd == leftReadFd)
						len = readn(leftReadFd,&buf);
					else if(fd == rightReadFd)
						len = readn(rightReadFd,&buf);
					buf[len] = '\0';
					temp = buf;
					pch = strtok (temp,":");
 					hopsCount = atoi(pch);
					pch = strtok(NULL,":");
					hopsCount--;
					if(hopsCount <= 0){
						isClose = 1;
						break;
					}
					
					potatoBuf = (char *)malloc( sizeof(char) * (len+5));
					sprintf(potatoBuf,"%d:%s%d:",hopsCount,pch,playerNo);
					random_number = rand() % 2;
					if(random_number == 0)	
						printf("Sending potato to %d\n",leftPlayerNo);
					else
						printf("Sending potato to %d\n",rightPlayerNo);	
					sendPotatoToPlayer(random_number,potatoBuf);
					free(buf);
					free(potatoBuf);
					fflush(stdout);		
				break;	
	
		}
		if(isClose){
			potatoBuf = (char *)malloc( sizeof(char) * (len+5));
			sprintf(potatoBuf,"%d:%s%d:",hopsCount,pch,playerNo);
			lastSocket = connectTo(argv,masterPort);  
			printf("I'm It\n");
			len = writen(lastSocket,potatoBuf,strlen(potatoBuf)); 
			fflush(stdout);
			free(buf);
			free(potatoBuf);
		}
		fflush(stdout);	
	}
	
}


/*
* Function to connect to the left and right neighbours. 
*
* Input  : void.
*
* Output : void.
*/
void connectToNeighbours(){
	char sstr[64];
	leftConnectSocket = connectTo(leftHostName,leftListenPort);
	rightConnectSocket = connectTo(rightHostName,rightListenPort);
}


/*
* Function to accept connection request from the neighbours. 
*
* Input  : listenSocket (Socket on which player is listening)
*
* Output : void
*/
void receiveRequestFromNeighbours(int listenSocket){
	struct sockaddr_in incoming;
	int len1 = len1 = sizeof( (struct sockaddr *) &incoming);
	rightReadFd = accept(listenSocket, (struct sockaddr *)&incoming, &len1);
	leftReadFd = accept(listenSocket, (struct sockaddr *)&incoming, &len1);
}



/*
* Function to send the potato to one of the neighbours. 
*
* Input  : side(left or right), potatoBuf(Potato data)
*
* Output : void.
*/
void sendPotatoToPlayer(int side,char *potatoBuf){
	int optval = 1;
	if(side == 0){
		writen(leftConnectSocket,potatoBuf,strlen(potatoBuf));
	}else if(side == 1){
		writen(rightConnectSocket,potatoBuf,strlen(potatoBuf));
	}
}


/*
* Function to parse the control(Players) Information received from the master. 
*
* Input  : character array containing the information.
*
* Output : void.
*/
void parseString(char *buf){
	char *pch;
	pch = strtok (buf,";");
 	playerNo = atoi(pch);

	//Info about the left Neighbour
	pch = strtok (NULL, ";");
	leftPlayerNo = atoi(pch);
	pch = strtok (NULL, ";");
	leftHostName = (char *)malloc(strlen(pch));
	memcpy(leftHostName, pch, strlen(pch));
	leftHostName[strlen(pch)]='\0';
	pch = strtok (NULL, ";");
	leftListenPort = atoi(pch);
	
	//Info about right neighbour
	pch = strtok (NULL, ";");
	rightPlayerNo = atoi(pch);
	pch = strtok (NULL, ";");
	rightHostName = (char *)malloc(strlen(pch));
	memcpy(rightHostName, pch, strlen(pch));
	rightHostName[strlen(pch)]='\0';
	pch = strtok (NULL, ";");
	rightListenPort = atoi(pch);

	printf("\nConnected as Player %d\n",playerNo);
}



/*
* Function to create a new Socket. 
*
* Input  : void.
*
* Output : void.
*/
int createSocket(){
	int s;
	/* use address family INET and STREAMing sockets (TCP) */
  	s = socket(AF_INET, SOCK_STREAM, 0);
  	if ( s < 0 ) {
    		perror("socket:");
    		exit(s);
  	}
	return s;
}


/*
* Function to start listening to a socket. 
*
* Input  : Socket to listen to.
*
* Output : port number on which listening.
*/
int startListening(int s){
	
	char buf[512];
  	char host[64];
  	struct sockaddr_in sin,foo;
	int rc,port=65400;
	int len = sizeof(struct sockaddr);
  	struct hostent *hp;

	/* fill in hostent struct for self */
  	gethostname(host, sizeof host);
  	hp = gethostbyname(host);
  	if ( hp == NULL ) {
    		fprintf(stderr, "Host not found (%s)\n",host);
    		exit(1);
  	}

  	/* set up the address and port */
  	//sin.sin_family = AF_INET;
  	//sin.sin_port = htons(port);
  	//memcpy(&sin.sin_addr, hp->h_addr_list[0], hp->h_length);
  
  	/* bind socket s to address sin */
	do{
		port++;  
		sin.sin_family = AF_INET;
  		sin.sin_port = htons(port);
  		memcpy(&sin.sin_addr, hp->h_addr_list[0], hp->h_length);		
		rc = bind(s, (struct sockaddr *)&sin, sizeof(sin));
		
  		//if ( rc < 0 ) {
    		//	perror("bind:");
    		//	exit(rc);
		//}
		
  	}while(rc<0);

  	rc = listen(s, 5);
  	if ( rc < 0 ) {
    		perror("listen:");
    		exit(rc);
 	 }

	//getsockname(s,(struct sockaddr *)&foo,&len);
	//port = ntohs(foo.sin_port);
	return port;
}


/*
* Function to connect  to a given host. 
*
* Input  : Hostname, port
*
* Output : Socket descriptor.
*/
int connectTo(char *host,int masterPort){
	struct hostent *hp;
	struct sockaddr_in sin;
	int s,len;
	int rc;
	
	char* addr = malloc (500*sizeof(char));  
	strcpy (addr, host);
	
	/* fill in hostent struct */
  	hp = (struct hostent *) gethostbyname(addr); 
  	if ( hp == NULL ) {
    		fprintf(stderr, "Host not found (%s)\n", host);
    		exit(1);
  	}

	/* create and connect to a socket */

  	/* use address family INET and STREAMing sockets (TCP) */
 	s = socket(AF_INET, SOCK_STREAM, 0);
  	if ( s < 0 ) {
    		perror("socket:");
    		exit(s);
  	}

	/* set up the address and port */
  	sin.sin_family = AF_INET;
  	sin.sin_port = htons(masterPort);
  	memcpy(&sin.sin_addr, hp->h_addr_list[0], hp->h_length);

	/* connect to socket at above addr and port */
  	rc = connect(s, (struct sockaddr *)&sin,sizeof(sin));
  	if ( rc < 0 ) {
    		perror("connect:");
    		exit(rc);
  	}
	free(addr);
	return s;
}

/*
* Function to read data size to the player. This function will inform the player about the total bytes that 
* sender is going to send so that the player can read same number of bytes.
*
* Input  : str(Data that is to be sent) , s(Socket descriptor of the player).
*
* Output : Total number of bytes sent.
*/
size_t sendSize(char *str,int s){
	char *size;	
	u_long packet_size;
	int len;
	packet_size = htonl(strlen(str)); // 10 bytes packet
	len = send(s,&packet_size, 4, 0); // First send the frame size
	return len;
}


/*
* Function to receive the size info sent by the player/master. 
*
* Input  : s(Socket descriptor of the player).
*
* Output : Size of incoming data.
*/
size_t receiveSize(int s){
	u_long packet_size;
	int bytes_to_read = 4; 
	int nresult;
	char *psize = (char *)&packet_size; 

	while(bytes_to_read) // Keep reading until we have all the bytes for the size
	{
		  nresult = recv(s, psize, bytes_to_read, 0);
		  bytes_to_read -= nresult;
		  psize += nresult;
	}
	packet_size = ntohl(packet_size);
	return packet_size;
}



/*
* Function to read N bytes of data using read call.
*
* Input  : fd(Socket from where to read) , vptr(buffer in which the data will be stored).
*
* Output : Total number of bytes read.
*/
ssize_t readn(int fd, char **vptr)
  {
      size_t  nleft;
      ssize_t nread;
      char   *ptr,*p;
	size_t n;
        n = receiveSize(fd);
	ptr = (char *)malloc( sizeof(char) * (n+5) );
	*vptr = ptr;
      
	
	nleft = n;
     while (nleft > 0) {
         if ( (nread = read(fd, ptr, nleft)) < 0) {
             if (errno == EINTR)
                 nread = 0;      /* and call read() again */
             else
                 return (-1);
         } else if (nread == 0)
             break;              /* EOF */

         nleft -= nread;
         ptr += nread;
     }
	
     return n;         /* return >= 0 */
 }


/*
* Function to write N bytes of data using write call.
*
* Input  : fd(Socket where to write) , vptr(buffer in which the data is present), n(Number of bytes to write).
*
* Output : Total number of bytes written.
*/
ssize_t writen(int fd, char *vptr, size_t n)
  {
      size_t nleft;
      ssize_t nwritten;
      char *ptr;

      ptr = vptr;
      nleft = n;
      sendSize(ptr,fd);
      while (nleft > 0) {
         if ( (nwritten = write(fd, ptr, nleft)) <= 0) {
             if (nwritten < 0 && errno == EINTR)
                 nwritten = 0;   /* and call write() again */
             else
                 return (-1);    /* error */
          }

          nleft -= nwritten;
          ptr += nwritten;
     }
     return (n);
 }


/*
* Function that takes the N socket and listen to them using select call. When request arrives, return the descriptor.
*
* Input  : fds[](Array of socket descriptors), count(Size of array).
*
* Output : Descriptor of the socket on which a request has arrived.
*/
int network_accept_any(int fds[], unsigned int count,struct sockaddr *addr, socklen_t *addrlen)
{
    	fd_set readfds;
    	int maxfd, fd;
    	unsigned int i;
    	int status;

    	FD_ZERO(&readfds);
    	maxfd = -1;
    	for (i = 0; i < count; i++) {
        	FD_SET(fds[i], &readfds);
        	if (fds[i] > maxfd)
            	maxfd = fds[i];
    	}
    	status = select(maxfd + 1, &readfds, NULL, NULL, NULL);
    	if (status < 0)
        	return INVALID_SOCKET;
    	fd = INVALID_SOCKET;
    	for (i = 0; i < count; i++)
        	if (FD_ISSET(fds[i], &readfds)) {
            		fd = fds[i];
            		break;
        	}
    	if (fd == INVALID_SOCKET)
        	return INVALID_SOCKET;
    	else
		return fd;
}




	
