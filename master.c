/*
*  Project 3 : Hot Potato
*
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <errno.h>
#define LEN 64
#define INVALID_SOCKET -1
int playerNo=0;

// Structure for storing the player information at the master.
struct Player{
	int playerNo;
	char *hostName;
	int listeningPortNo;
	int commFd;
	struct Player *next;
	struct Player *prev;
};

typedef struct Player Player;

Player *head=NULL;
Player *tail=NULL;


//Function Declaration
int startListening(int,struct hostent*);
Player* joinNewPlayer(int playerNo,char *,int masterPort,int p);
void appendToPlayerList(Player *player);
void playerToString(Player *pl);
size_t sendSize(char *str,int s);
size_t receiveSize(int s);
ssize_t readn(int fd, char **vptr);
ssize_t writen(int fd, char *vptr, size_t n);
void throwPotato(int playerCount,int hopsCount);
void catchPotato(int s);


//Main
int main (int argc, char *argv[]){
  	Player player,*pl;
	char host[64];
	char *buf;
  	int s, p, fp, rc, len, port[4000],hopsCount,playerCount,masterPort;
	int i;
  	int fds[2];
	struct sockaddr_in sin,incoming;
	struct hostent *hp, *ihp;
	char str[INET_ADDRSTRLEN];

  	/* read port number from command line */
  	if ( argc < 4 ) {
    		fprintf(stderr, "Usage: %s <port-number> <number-of-players> <hops>\n", argv[0]);
    		exit(1);
  	}
  	hopsCount = atoi(argv[3]);
	if(hopsCount < 0){
		fprintf(stderr,"Hops must be positive.\n");  	
		return 0;
	}
	playerCount = atoi(argv[2]);
	if(playerCount <1){
		fprintf(stderr,"The minimum number of players must be 2.\n");  	
		return 0;
	}
	masterPort = atoi(argv[1]);

	/* fill in hostent struct for self */
  	gethostname(host, sizeof host);
  	hp = gethostbyname(host);
  	if ( hp == NULL ) {
    		fprintf(stderr, "%s: host not found (%s)\n", argv[0], host);
    		exit(1);
  	}

	printf("\nPotato Master on %s",host);
	printf("\nPlayers = %d",playerCount);
	printf("\nHops = %d\n\n",hopsCount);

	fflush(stdout);
	s = startListening(masterPort,hp);		 //s  is the socket on which master is listening.
	
  	/* accept connections */
  	for(i=0;i<playerCount;i++) {
    		len = sizeof(sin);
   		p = accept(s, (struct sockaddr *)&incoming, &len);
    		if ( p < 0 ) {
      			perror("bind:");
      			exit(rc);
    		}
		readn(p,&buf);
    		ihp = gethostbyaddr((char *)&incoming.sin_addr, sizeof(struct in_addr), AF_INET);
		inet_ntop( AF_INET, &(incoming.sin_addr), str, INET_ADDRSTRLEN );
		joinNewPlayer(playerNo,ihp->h_name,atoi(buf),p);
		printf("Player %d is on %s \n",playerNo,ihp->h_name);
		playerNo++;
		free(buf);
	}
	pl = head;

	//Start the ring construction
	initiateRingConstruction();
	
	//Throws the potato to one of the player.
	if(hopsCount>0){
		throwPotato(playerCount,hopsCount);
		//When the game finishes, receive potato from the player.
		catchPotato(s);
	}
	//Terminate all the player processes.
	pl = head;
	buf = "close";
	while(pl!=NULL){
		writen(pl->commFd,buf,strlen(buf));
    		close(pl->commFd);
		pl = pl->next;
	}
	
    	//printf(">> Connection closed\n");
  	close(s);
 	exit(0);
}


/*
* This function accepts the reply from the player with the last hop.
*
* Input  : socket on which master is listening.
*
* Output : void
*/
void catchPotato(int s){
	int len,rc,i=0,p;
	char *buf;
	char *pch;
	struct sockaddr_in incoming;
	len = sizeof(incoming);
   	p = accept(s, (struct sockaddr *)&incoming, &len);
	if ( p < 0 ) {
      		perror("bind:");
      		exit(rc);
    	}
	len = readn(p,&buf);
	buf[len]='\0';
	pch = strtok (buf,":");
	pch = strtok(NULL,":");
	printf("\nTrace of Potato:\n");
	while(pch[i]!='\0'){
		printf("%c,",pch[i]);
		i++;
		
	}
	printf("\n");
	fflush(stdout);
}


/*
* This function throws the potato to a randomly choosen player.
*
* Input  : playerCount(Total number of players) , hopsCount(Total hops).
*
* Output : void
*/
void throwPotato(int playerCount,int hopsCount){
	int random_number,i;
	Player *pl = head;
	char str[LEN];
	//srand(1);
	random_number = rand() % playerCount;
	printf("\n\nAll Players Present, sending potato to player %d\n",random_number);
	
	for(i=0;i<random_number;i++)
		pl = pl->next;

	sprintf(str,"%d::",hopsCount);
	writen(pl->commFd,str,strlen(str));
	fflush(stdout);
}


/*
* This function starts the process of ring construction. Each player is told about his left neighbour and right neighbour.
*
* Input  : void.
*
* Output : void
*/
int initiateRingConstruction(){

	Player *p;
	p = head;
	char str[512],size[32];
	int len,playerNo,leftPort,rightPort,leftPlayerNo,rightPlayerNo;
	char *leftHostName,*rightHostName;
	u_long packet_size;
	while(p!=NULL){
		playerNo = p->playerNo;
		if(p->prev==NULL){
			leftPort = tail->listeningPortNo;
			leftHostName = tail->hostName;
			leftPlayerNo = tail->playerNo;
		}
		else{
			leftPort = p->prev->listeningPortNo;
			leftHostName = p->prev->hostName;
			leftPlayerNo = p->prev->playerNo;
		}

		if(p->next==NULL){
			rightHostName = head->hostName;
			rightPort = head->listeningPortNo;
			rightPlayerNo = head->playerNo;
		}
		else{
			rightHostName = p->next->hostName;
			rightPort = p->next->listeningPortNo;
			rightPlayerNo = p->next->playerNo;
 		}
		sprintf(str,"%d;%d;%s;%d;%d;%s;%d",playerNo,leftPlayerNo,leftHostName,leftPort,rightPlayerNo,rightHostName,rightPort);
		len = writen(p->commFd,str,strlen(str));
		p = p->next;	
	}
	
}


/*
* Function to read N bytes of data using read call.
*
* Input  : fd(Socket from where to read) , vptr(buffer in which the data will be stored).
*
* Output : Total number of bytes read.
*/
ssize_t readn(int fd, char **vptr)
  {
      size_t  nleft;
      ssize_t nread;
      char   *ptr;
      size_t n = receiveSize(fd);
	ptr = (char *)malloc( sizeof(char) * (n+5) );
	*vptr = ptr;
	nleft = n;
     	while (nleft > 0) {
         if ( (nread = read(fd, ptr, nleft)) < 0) {
             if (errno == EINTR)
                 nread = 0;      /* and call read() again */
             else
                 return (-1);
         } else if (nread == 0)
             break;              /* EOF */

         nleft -= nread;
         ptr += nread;
     }
     return n;         /* return >= 0 */
 }


/*
* Function to write N bytes of data using write call.
*
* Input  : fd(Socket where to write) , vptr(buffer in which the data is present), n(Number of bytes to write).
*
* Output : Total number of bytes written.
*/
ssize_t writen(int fd, char *vptr, size_t n)
  {
      size_t nleft;
      ssize_t nwritten;
      char *ptr;

      ptr = vptr;
      nleft = n;
      sendSize(ptr,fd);
      while (nleft > 0) {
         if ( (nwritten = write(fd, ptr, nleft)) <= 0) {
             if (nwritten < 0 && errno == EINTR)
                 nwritten = 0;   /* and call write() again */
             else
                 return (-1);    /* error */
          }

          nleft -= nwritten;
          ptr += nwritten;
     }
     return (n);
 }


/*
* Function to read data size to the player. This function will inform the player about the total bytes that 
* sender is going to send so that the player can read same number of bytes.
*
* Input  : str(Data that is to be sent) , s(Socket descriptor of the player).
*
* Output : Total number of bytes sent.
*/
size_t sendSize(char *str,int s){
	char *size;	
	u_long packet_size;
	int len;
	packet_size = htonl(strlen(str)); 
	len = send(s,&packet_size, 4, 0); // First send the frame size
	return len;
}


/*
* Function to receive the size info sent by the player/master. 
*
* Input  : s(Socket descriptor of the player).
*
* Output : Size of incoming data.
*/
size_t receiveSize(int s){
	u_long packet_size;
	int bytes_to_read = 4; 
	int nresult; 
	char *psize = (char *)&packet_size; // Point to first byte of size

	while(bytes_to_read) // Keep reading until we have all the bytes for the size
	{
		  nresult = recv(s, psize, bytes_to_read, 0);
		  bytes_to_read -= nresult;
		  psize += nresult;
	}
	packet_size = ntohl(packet_size);
	return packet_size;
}



/*
* This function add a new player in the list.
*
* Input  : PlayerNo, Hostname, PortNumber on which player is listening, commFd is the communication socket between player and master.
*
* Output : Pointer to newly created player.
*/
Player* joinNewPlayer(int playerNo,char *hostName,int port,int commFd){
	Player *player;
	player = (Player *)malloc(sizeof(Player));
	player->playerNo = playerNo;
	player->hostName = (char *)malloc(strlen(hostName));
	memcpy(player->hostName, hostName, strlen(hostName));
	//player->hostName = hostName;
	player->listeningPortNo = port;
	player->commFd = commFd;
	player->next = NULL;
	player->prev = NULL;
	appendToPlayerList(player);
	return player;
}


void appendToPlayerList(Player *player){
	Player *p;
	if(head==NULL){
		head = player;
		tail = player;
	}
	else{
		p = player;
		tail->next = p;
		p->prev = tail;
		tail = p;
	}
		
}


void playerToString(Player *pl){
	
	printf("\nPlayerNo :: %d",pl->playerNo);
	printf("\nHostName :: %s",pl->hostName);
	printf("\nListeningPortNo :: %d",pl->listeningPortNo);
	printf("\nCommFd :: %d",pl->commFd);	
}


/*
* Function to listen to a particular port.
*
* Input  : port , hp.
*
* Output : Socket on which listening.
*/
int startListening(int port,struct hostent *hp){
	
	char buf[512];
  	char host[64];
  	struct sockaddr_in sin, incoming;
	int s,rc;
	
  	/* use address family INET and STREAMing sockets (TCP) */
  	s = socket(AF_INET, SOCK_STREAM, 0);
  	if ( s < 0 ) {
    		perror("socket:");
    		exit(s);
  	}

  	/* set up the address and port */
  	sin.sin_family = AF_INET;
  	sin.sin_port = htons(port);
  	memcpy(&sin.sin_addr, hp->h_addr_list[0], hp->h_length);
  
  	/* bind socket s to address sin */
  	rc = bind(s, (struct sockaddr *)&sin, sizeof(sin));
  	if ( rc < 0 ) {
    		perror("bind:");
    		exit(rc);
  	}

  	rc = listen(s, 5);
  	if ( rc < 0 ) {
    		perror("listen:");
    		exit(rc);
 	 }

	return s;
}



/*
* Function that takes the N socket and listen to them using select call. When request arrives, perform accept on the incoming request.
*
* Input  : fds[](Array of socket descriptors), count(Size of array).
*
* Output : Descriptor of the new connection socket.
*/
int network_accept_any(int fds[], unsigned int count,struct sockaddr *addr, socklen_t *addrlen)
{
    	fd_set readfds;
    	int maxfd, fd;
    	unsigned int i;
    	int status;

    	FD_ZERO(&readfds);
    	maxfd = -1;
    	for (i = 0; i < count; i++) {
        	FD_SET(fds[i], &readfds);
        	if (fds[i] > maxfd)
            	maxfd = fds[i];
    	}
    	status = select(maxfd + 1, &readfds, NULL, NULL, NULL);
    	if (status < 0)
        	return INVALID_SOCKET;
    	fd = INVALID_SOCKET;
    	for (i = 0; i < count; i++)
        	if (FD_ISSET(fds[i], &readfds)) {
            		fd = fds[i];
            		break;
        	}
    	if (fd == INVALID_SOCKET)
        	return INVALID_SOCKET;
    	else
        	return accept(fd, addr, addrlen);
}


